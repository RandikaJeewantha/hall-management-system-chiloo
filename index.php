<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>

<?php 
    $query = "SELECT * FROM emty_halls";
	$result_set = mysqli_query($connection, $query);
	verify_query($result_set); 
?>

<?php 
    user_login();
    admin_login() 
?>

<?php 

    $text = "";

    if(isset($_POST['searchhallsubmit']) && !empty($_POST['searchHall'])) {

		$errors = array();
		
		if (!isset($_POST['searchHall']) || strlen(trim($_POST['searchHall'])) < 1)  {
			$errors[] = "Hall is Missing / Invalid";
		}

		$hall = mysqli_real_escape_string($connection, $_POST['searchHall']);

		$querysearchinghall = "SELECT * FROM halls WHERE Hall_Name = '{$hall}'";
        
        $result_setsearchinghall = mysqli_query($connection, $querysearchinghall);
        
        verify_query($result_setsearchinghall);
       
        $searchinghallcount = mysqli_num_rows($result_setsearchinghall);
        
        if ($searchinghallcount == 0) {
            $errors[] = "There are no data about you entered Hall or Typing Error";
        }
		
		if (!empty($errors)) {
            
			$err = "";
	  
			foreach ($errors as $error) {
			  $err .= $error;
			  $err .= "  ";
			}
	  
			echo "<script type='text/javascript'>alert('$err');</script>";
			header("Refresh: 0");
		}
	  
		else {
			$queryh = "SELECT * FROM emty_halls WHERE Hall_Name = '{$hall}' ";
			$result_seth = mysqli_query($connection, $queryh);
			verify_query($result_seth);
			$result_sethcount = mysqli_num_rows($result_seth);
			
			if ($result_sethcount == 1 ) {
				$text = "Vacant !";
			}
			else{
				$text = "Occupied !";
			}
		}
	} 
?>

<!DOCTYPE html>
<html lang="en"> 

    <head>
        <title>HOME</title>
        <meta charset="UTF-8"> 
        <meta name="viewport" content="width=device-width,  user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="asserts/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>

        <?php require_once 'includes/adminLog.php'; ?>
        <?php require_once 'includes/userLogin.php'; ?>
        <?php require_once 'includes/nav.php'; ?>


        <?php 
            if(isset($text) && strlen(trim($text)) > 1){

                echo '
                    <div class="container mt-3 d-flex justify-content-center">
                        <div class="card text-center" style="width: 50rem"> '
                ;

                if($text == "Occupied !"){

                    echo '
                        <div class="alert alert-warning">
                            <strong>Occupied !</strong><br>'.$hall. '&nbsp;&nbsp;&nbsp;This Hall.
                        </div>'
                    ;
                }

                if($text == "Vacant !"){
                            
                    echo '
                        <div class="alert alert-info">
                            <strong>Vacant !</strong><br>'.$hall. '&nbsp;&nbsp;&nbsp;This Hall.
                        </div>'
                    ;
                }
                        
                echo '
                        </div>
                    </div>';
            }
        ?>
            
        
        <div class="container mt-3 d-flex justify-content-center" >

            <div class="card text-center bg-dark" style="width: 50rem;">
            
                <div class="card-header text-white"> Empty Halls </div>
    
                <div class="card-body text-black">
                    <ul class="list-group list-group-flush">
                        <?php 
                            while ($data = mysqli_fetch_assoc($result_set)){       
                                echo '<li class="list-group-item">'.$data["Hall_Name"].'</li>';   
                            }
                        ?>
                    </ul>
                </div>
    
            </div>
        </div>

        <?php require_once 'includes/footer.php'; ?>
    </body>
</html>

 <?php mysqli_close($connection); ?>