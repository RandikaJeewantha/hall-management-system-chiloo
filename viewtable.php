<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php user_login(); ?>

<?php

	if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {
		echo "<script type='text/javascript'>alert('You cant access this page');</script>";
		echo "<script>setTimeout(\"location.href = 'index.php';\",0);</script>";
	}

?>

<?php 
  
  $query = "SELECT * FROM `new_arrangement` ORDER BY `new_arrangement`.`Date` ASC";
	$result_set = mysqli_query($connection, $query);
	verify_query($result_set); 

?>

<?php 
  
  $query2 = "SELECT * FROM `default_arrangement` ORDER BY `default_arrangement`.`Day` ASC";
	$result_set2 = mysqli_query($connection, $query2);
	verify_query($result_set2); 

?>

<!DOCTYPE html>
<html lang="en"> 

  <head>
    <title>HOME</title>
    <?php require_once 'includes/header.php'; ?>
  </head>

  <body>

    <?php require_once 'includes/adminLog.php' ?>
    <?php require_once 'includes/userLogin.php' ?>
    <?php require_once 'includes/nav.php'; ?>

    <div class="container mt-3 d-flex justify-content-center" >
      <div class="row mt-3 mb-3">
          
        <form action="sheduleSRH.php" method="POST" class="form-inline">
				
          <input type="date" class="form-control mr-2" name="inputdate"  placeholder="mm/dd/yyyy">
				  <button class="btn btn-dark my-2 my-sm-0" type="submit" name = "sheduleSearch" data-toggle="modal" data-target="">Search Date</button>
			    
        </form>

      </div>
    </div>

    <div class="container mt-3 d-flex justify-content-center" >
      <div class="row mt-3 mb-3">
        <h3><b>New Arrangement Table</b> </h3> 
      </div>
    </div>

    <div class="container mt-3 d-flex justify-content-center" >

      <table class="table table-secondary">
      
        <thead class="thead-dark">
          <tr>
            <th scope="col">Date</th>
            <th scope="col">Start Time</th>
            <th scope="col">End Time</th>
            <th scope="col">Hall</th>
            <th scope="col">Lecturer</th>
            <th scope="col">Subject Code</th>
            <th scope="col">Department</th>
          </tr>
        </thead>
  
        <tbody>

          <?php 
            while ($data = mysqli_fetch_assoc($result_set)){       
              echo '
                <tr>
                  <td>'.$data["Date"].'</td>
                  <td>'.$data["Start_Time"].'</td>
                  <td>'.$data["End_Time"].'</td>
                  <td>'.$data["Hall_Name"].'</td>
                  <td>'.$data["Lecturer"].'</td>
                  <td>'.$data["subject_code"].'</td>
                  <td>'.$data["Department"].'</td>
                </tr>';
            }
          ?>

        </tbody>
      </table>
      
    </div>

    <br>

    <div class="container mt-3 d-flex justify-content-center" >
      <div class="row mt-3 mb-3">
        <h3><b>Default Arrangement Table</b> </h3> 
      </div>
    </div>

    <div class="container mt-3 d-flex justify-content-center" >

      <table class="table table-secondary">
      
        <thead class="thead-dark">
          <tr>
            <th scope="col">Date</th>
            <th scope="col">Start Time</th>
            <th scope="col">End Time</th>
            <th scope="col">Hall</th>
            <th scope="col">Lecturer</th>
            <th scope="col">Subject</th>
            <th scope="col">Department</th>
          </tr>
        </thead>
  
        <tbody>

          <?php 
            while ($data = mysqli_fetch_assoc($result_set2)){       
              echo '
                <tr>
                  <td>'.$data["Day"].'</td>
                  <td>'.$data["Start_Time"].'</td>
                  <td>'.$data["End_Time"].'</td>
                  <td>'.$data["Hall_Name"].'</td>
                  <td>'.$data["Lecturer"].'</td>
                  <td>'.$data["Subject"].'</td>
                  <td>'.$data["Department"].'</td>
                </tr>';
            }
          ?>

        </tbody>
      </table>
      
    </div>

    <?php require_once 'includes/footer.php'; ?>

  </body>
</html>