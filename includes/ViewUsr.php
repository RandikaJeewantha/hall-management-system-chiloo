<?php 
  $query = "SELECT * FROM users";
	$result_set = mysqli_query($connection, $query);
  verify_query($result_set);
  $count1 = mysqli_num_rows($result_set); 
?>

<?php
  
  if(isset($_POST['delete'])) {

    $demail = $_POST['delete'];
    
    $queryd = "DELETE FROM users WHERE Email = '{$demail}'";
	  $result_setd = mysqli_query($connection, $queryd);
    verify_query($result_setd);

    $querycount2 = "SELECT * FROM users";
    $result_setcount2 = mysqli_query($connection, $querycount2);
    verify_query($result_setcount2);
    $count2 = mysqli_num_rows($result_setcount2);
    $count_different = $count2 - $count1;

    if($count_different < 0) {
      echo "<script type='text/javascript'>alert('Sucessfull deleted !');</script>";
      echo "<script>setTimeout(\"location.href = '';\",0);</script>";
    }

    elseif($count_different > 0) {
      echo "<script type='text/javascript'>alert('Failed !');</script>";
    }
           
    else {
      echo "<script type='text/javascript'>alert('Invalid Event Occurred !');</script>";
    }
  }

?>

<!--view user-->
  <div class="modal" tabindex=-1 role="dialog" id="extuser">
	  <div class="modal-dialog modal-xl modal-dialog-scrollable " role="document">
		  <div class="modal-content">
			  <div class="modal-header bg-secondary text-white">
				
          <h4 class="modal-title">Users</h4>

				  <button type="button" class="close" data-dismiss="modal" area-label="close">

					  <span area-hidden="true">&times;</span>

				  </button>
			
        </div>

				<div class="modal-body">

          <form action="searchpage.php" method="POST" class="form-inline my-2 my-lg-0">

					  <input class="form-control mr-sm-2" type="search" name = "searchingMail" placeholder="enter e-mail" aria-label="Search" >
					  <!--<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">-->

            <button type="submit" name="search" class="btn btn-outline-dark" >Search</button>
          
          </form>
				  
          <br><br>


			    <table class="table text-center">
            
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Full Name</th>
                <th scope="col">Name With Initials</th>
                <th scope="col">Phone Number</th>
                <th scope="col">Email</th>
                <th scope="col">PIN</th>
                <th scope="col">Registration Number</th>
                <th scope="col">Department</th>
                <th scope="col">Faculty</th>
                <th scope="col">Year</th>
                <th scope="col">Position</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            
            <tbody>

              <?php
                
                $no = 0;
                while ($data = mysqli_fetch_assoc($result_set)){
                  $no = $no + 1;       
                  echo '
                    <tr>
                      <th scope="row">'.$no.'</th>
                      <td>'.$data["Full_Name"].'</td>
                      <td>'.$data["Name_With_Initials"].'</td>
                      <td>'.$data["Phone_Number"].'</td>
                      <td>'.$data["Email"].'</td>
                      <td>'.$data["PIN"].'</td>
                      <td>'.$data["Registration_Number"].'</td>
                      <td>'.$data["Department"].'</td>
                      <td>'.$data["Faculty"].'</td>
                      <td>'.$data["Year"].'</td>
                      <td>'.$data["Position"].'</td>
                      
                      <td>
                        <form action="userEdit.php" method="post">
                          <button type="submit" name="edit" class="btn btn-outline-dark" value='.$data["Id"].'>edit</button>
                        </form>
                      </td>

                      <td>
                        <form action="" method="post">
                          <button type="submit" name="delete" class="btn btn-outline-dark" value='.$data["Email"].'>Delete</button>
                        </form>
                      </td>
                    </tr>'
                  ;
                }
              ?>

            </tbody>
          </table>

        </div>

        <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">close</button>
				</div>

			</div>
		</div>
	</div>

	<!--view user over-->