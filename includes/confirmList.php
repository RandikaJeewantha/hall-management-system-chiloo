<?php 
  $query = "SELECT * FROM `new_arrangement` WHERE isConformed = 0 ORDER BY `new_arrangement`.`Date` ASC";
	$result_set = mysqli_query($connection, $query);
  verify_query($result_set); 
  $countl1 = mysqli_num_rows($result_set); 
?>

<?php 

  if(isset($_POST['confirm'])) {

    $id = $_POST['confirm'];

    $query = "UPDATE new_arrangement SET isConformed = 1 WHERE Id = '{$id}' LIMIT 1 ";

    $result_setreg = mysqli_query($connection, $query);
    
    $is = verify_query($result_setreg);

    if($is) {
			echo "<script type='text/javascript'>alert('Successfull confirmed !');</script>";
			echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
		}    
    else {
			echo "<script type='text/javascript'>alert('Failed !');</script>";
			echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
    }
  }

?>

<?php 

  if(isset($_POST['cansel'])) {

    $idd = $_POST['cansel'];

    $queryd = "DELETE FROM new_arrangement WHERE Id = '{$idd}' LIMIT 1 ";

    $result_setregd = mysqli_query($connection, $queryd);
    
    $isd = verify_query($result_setregd);

    $queryl2 = "SELECT * FROM `new_arrangement` WHERE isConformed = 0 ORDER BY `new_arrangement`.`Date` ASC";
	  $result_setl2 = mysqli_query($connection, $queryl2);
    verify_query($result_setl2); 
    $countl2 = mysqli_num_rows($result_setl2); 

    $dif = $countl1 - $countl2;

    if($dif > 0) {
			echo "<script type='text/javascript'>alert('Successfull Cancelled !');</script>";
			echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
		}    
    else {
			echo "<script type='text/javascript'>alert('Failed !');</script>";
			echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
    }
  }

?>

<div class="modal" tabindex="-1" role="dialog" id="request">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        
        <h5 class="modal-title">Requests</h5>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        
        </button>
      </div>

      <div class="modal-body">

        <table class="table table-striped table-secondary">
          
          <thead>
            <tr>
              <th scope="col">Date</th>
              <th scope="col">Start Time</th>
              <th scope="col">End Time</th>
              <th scope="col">Hall</th>
              <th scope="col">Lecturer</th>
              <th scope="col">Subject Code</th>
              <th scope="col">Department</th>
              <th scope="col">Decision</th>
            </tr>
          </thead>

          <tbody>
            <?php 
              while ($data = mysqli_fetch_assoc($result_set)){       
                echo '
                  <tr>
                    <td>'.$data["Date"].'</td>
                    <td>'.$data["Start_Time"].'</td>
                    <td>'.$data["End_Time"].'</td>
                    <td>'.$data["Hall_Name"].'</td>
                    <td>'.$data["Lecturer"].'</td>
                    <td>'.$data["subject_code"].'</td>
                    <td>'.$data["Department"].'</td>
                    <td>
                      <form action="" method="post">
                        <button type="submit" name="confirm" class="btn btn-primary mb-2" value='.$data["Id"].'>confirm</button>
                      </form>

                      <form action="" method="post">
                        <button type="submit" name="cansel" class="btn btn-danger" value='.$data["Id"].'>Cancel</button>
                      </form>
                    </td>
                  </tr>';
              }
            ?>
          </tbody>
        </table>

      </div>

      <div class="modal-footer bg-light">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>