<?php

    global $connection;

    $id = $_SESSION['Id'];

    $queryinfo = "SELECT * FROM admins WHERE Id = $id";
    $result_setinfo = mysqli_query($connection, $queryinfo);
    verify_query($result_setinfo);
    $info = mysqli_num_rows($result_setinfo);
    
    if($info == 1) {
       
        while ($data = mysqli_fetch_assoc($result_setinfo)){       
            
            $full_name = $data["Full_Name"];
            $initials = $data["Name_With_Initials"]; 
            $address= $data["Address"]; 
            $email = $data["Email"]; 
            $pass = $data["Password"]; 
            $position = $data["Position"]; 
            $number = $data["Phone_number"];    
        }
    }

    else {
        echo "<script type='text/javascript'>alert('Connection Error');</script>";
    }
?>

<?php 

    $querycount1 = "SELECT * FROM admins";
    $result_setcount1 = mysqli_query($connection, $querycount1);
    verify_query($result_setcount1);
    $count1 = mysqli_num_rows($result_setcount1);
?>

<?php 
   
    if(isset($_POST['Register'])) {
       
        $errors = array();

        if (!isset($_POST['inputname']) || strlen(trim($_POST['inputname'])) < 1)  {
            $errors[] = "Input Name is Missing / Invalid";
        }

        if (!isset($_POST['inputinitial']) || strlen(trim($_POST['inputinitial'])) < 1)  {
            $errors[] = "Initial with Name is Missing / Invalid";
        }

        if (!isset($_POST['inputAddress']) || strlen(trim($_POST['inputAddress'])) < 1)  {
            $errors[] = "Address is Missing / Invalid";
        }

        if (!isset($_POST['inputEmail']) || strlen(trim($_POST['inputEmail'])) < 1)  {
            $errors[] = "Email is Missing / Invalid";
        }

        if (!isset($_POST['inputPassword']) || strlen(trim($_POST['inputPassword'])) < 1)  {
            $errors[] = "Password is Missing / Invalid";
        }

        if (!isset($_POST['inputPositon']) || strlen(trim($_POST['inputPositon'])) < 1)  {
            $errors[] = "Position is Missing / Invalid";
        }

        if (!isset($_POST['inputphone']) || strlen(trim($_POST['inputphone'])) < 1)  {
            $errors[] = "Phone Number is Missing / Invalid";
        }
        
        if (isset($_POST['inputEmail']) && strlen(trim($_POST['inputEmail'])) > 1) {
            
            $searchemail = $_POST['inputEmail'];
        
            $querysearchemail = "SELECT * FROM admins WHERE Email = '{$searchemail}'";
        
            $result_setsearchemail = mysqli_query($connection, $querysearchemail);
        
            verify_query($result_setsearchemail);
       
            $searchemailcount = mysqli_num_rows($result_setsearchemail);
        
            if ($searchemailcount > 0) {
                $errors[] = "Email is Already Exist";
            }
        }

        if (empty($errors)) {

            $inputname = mysqli_real_escape_string($connection, $_POST['inputname']);
            $inputinitial = mysqli_real_escape_string($connection, $_POST['inputinitial']);
            $inputAddress = mysqli_real_escape_string($connection, $_POST['inputAddress']);
            $inputEmail = mysqli_real_escape_string($connection, $_POST['inputEmail']);
            $inputPassword = mysqli_real_escape_string($connection, $_POST['inputPassword']);
            $inputPositon = mysqli_real_escape_string($connection, $_POST['inputPositon']);
            $inputphone = mysqli_real_escape_string($connection, $_POST['inputphone']);
            
            $query = "INSERT INTO admins (Full_Name, Name_With_Initials, Address, Email, Password, Position, Phone_number)
                VALUES ('{$inputname}', '{$inputinitial}', '{$inputAddress}', '{$inputEmail}', '{$inputPassword}', '{$inputPositon}', '{$inputphone}') LIMIT 1 ";

            $result_setreg = mysqli_query($connection, $query);
            verify_query($result_setreg);

            $querycount2 = "SELECT * FROM admins";
            $result_setcount2 = mysqli_query($connection, $querycount2);
            verify_query($result_setcount2);
            $count2 = mysqli_num_rows($result_setcount2);
            $count_different = $count2 - $count1;

            if($count_different < 0) {
                echo "<script type='text/javascript'>alert('Failed !');</script>";
            }

            elseif($count_different > 0) {
                echo "<script type='text/javascript'>alert('Sucessfull Added !');</script>";
            }
           
            else {
                $errors[] = 'Invalid Event Occurred';
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "  ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }

?>

<?php 
   
    if(isset($_POST['Modify'])) {
       
        $errors = array();

        if (isset($_POST['inputname']) || strlen(trim($_POST['inputname'])) > 1)  {
            $inputname = mysqli_real_escape_string($connection, $_POST['inputname']);
        }

        if (isset($_POST['inputinitial']) || strlen(trim($_POST['inputinitial'])) > 1)  {
            $inputinitial = mysqli_real_escape_string($connection, $_POST['inputinitial']);
        }

        if (isset($_POST['inputAddress']) || strlen(trim($_POST['inputAddress'])) > 1)  {
            $inputAddress = mysqli_real_escape_string($connection, $_POST['inputAddress']);
        }

        if (isset($_POST['inputEmail']) || strlen(trim($_POST['inputEmail'])) > 1)  {
            
            $searchemail = $_POST['inputEmail'];
        
            $querysearchemail = "SELECT * FROM admins WHERE Email = '{$searchemail}'";
        
            $result_setsearchemail = mysqli_query($connection, $querysearchemail);
        
            verify_query($result_setsearchemail);
       
            $searchemailcount = mysqli_num_rows($result_setsearchemail);
        
            if ($searchemailcount > 0) {
                $errors[] = "Email is Already Exist";
            }

            else {
                $inputEmail = mysqli_real_escape_string($connection, $_POST['inputEmail']);
            }
        }

        if (isset($_POST['inputPassword']) || strlen(trim($_POST['inputPassword'])) > 1)  {
            $inputPassword = mysqli_real_escape_string($connection, $_POST['inputPassword']);
        }

        if (isset($_POST['inputPositon']) || strlen(trim($_POST['inputPositon'])) > 1)  {
            $inputPositon = mysqli_real_escape_string($connection, $_POST['inputPositon']);
        }

        if (isset($_POST['inputphone']) || strlen(trim($_POST['inputphone'])) > 1)  {
            $inputphone = mysqli_real_escape_string($connection, $_POST['inputphone']);
        }

        if (empty($errors)) {

            if (!isset($inputname) || strlen(trim($_POST['inputname'])) < 1)  {
                $inputname = $full_name;
            }

            if (!isset($inputinitial) || strlen(trim($_POST['inputinitial'])) < 1)  {
                $inputinitial = $initials;
            }

            if (!isset($inputAddress) || strlen(trim($_POST['inputAddress'])) < 1)  {
                $inputAddress = $address;
            }

            if (!isset($inputEmail) || strlen(trim($_POST['inputEmail'])) < 1)  {
                $inputEmail = $email;
            }  
           
            if (!isset($inputPassword) || strlen(trim($_POST['inputPassword'])) < 1)  {
                $inputPassword = $pass;
            }

            if (!isset($inputPositon) || strlen(trim($_POST['inputPositon'])) < 1)  {
                $inputPositon = $position;
            }

            if (!isset($inputphone) || strlen(trim($_POST['inputphone'])) < 1)  {
                $inputphone = $number;
            }
            
            $query = "UPDATE admins SET Full_Name = '{$inputname}', Name_With_Initials = '{$inputinitial}', Address = '{$inputAddress}', 
                Email = '{$inputEmail}', Password = {$inputPassword}, Position = '{$inputPositon}', 
                Phone_number = '{$inputphone}' WHERE `admins`.`Id` = '{$id}' LIMIT 1 ";

            $result_setreg = mysqli_query($connection, $query);
            $status = verify_query($result_setreg);

            if($result_setreg==true) {
                echo "<script type='text/javascript'>alert('Sucessfully Updated !');</script>";
            }
           
            else {
                echo "<script type='text/javascript'>alert('Failed !');</script>";
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "  ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }

?>


<div class="modal" tabindex=-1 role="dialog" id="adminadd">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary text-white">
				
				<h4 class="modal-title">Admin Details</h4>

				<button type="button" class="close" data-dismiss="modal" area-label="close">

					<span area-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-body">
				
				<form action="" method="POST">

					<div class="form-group">
						<label for="name">Full name</label>
						<input type="text" class="form-control" name="inputname" placeholder="<?php echo $full_name; ?>">
					</div>

					<div class="form-group">
						<label for="inputinitial">Name with intials</label>
						<input type="text" class="form-control" name="inputinitial" placeholder="<?php echo $initials; ?>">
					</div>

					<div class="form-group">
						<label for="inputAddress">Address</label>
						<input type="text" class="form-control" name="inputAddress" placeholder="<?php echo $address; ?>">
					</div>

					<div class="form-row">

						<div class="form-group col-md-6">
							<label for="inputEmail4">Email</label>
							<input type="email" class="form-control" name="inputEmail" placeholder="<?php echo $email; ?>">
						</div>

						<div class="form-group col-md-6">
							<label for="inputPassword4">Password</label>
							<input type="password" class="form-control" name="inputPassword" placeholder="<?php echo $pass; ?>">
						</div>

					</div>

                    <div class="form-row">

						<div class="form-group col-md-6">
							<label for="inputPosition">Position</label>
							<input type="text" class="form-control" name="inputPositon" placeholder="<?php echo $position; ?>">
						</div>

						<div class="form-group col-md-6">
                        	<label for="inputphone">Phone number</label>
							<input type="text" class="form-control" name="inputphone" placeholder="<?php echo $number; ?>">
						</div>

					</div>
				
					<div class="modal-footer">
						<button type="submit" name="Register" class="btn btn-primary">Register</button>
                        <button type="submit" name="Modify" class="btn btn-primary">Modify</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>