<?php 
	global $connection;

	$querycount1 = "SELECT * FROM users";
    $result_setcount1 = mysqli_query($connection, $querycount1);
    verify_query($result_setcount1);
    $count1 = mysqli_num_rows($result_setcount1);
?>

<?php

	if(isset($_POST['student_Register'])) {
       
        $errors = array();

        if (!isset($_POST['inputname']) || strlen(trim($_POST['inputname'])) < 1)  {
            $errors[] = "Input Name is Missing / Invalid";
        }

        if (!isset($_POST['inputinitials']) || strlen(trim($_POST['inputinitials'])) < 1)  {
            $errors[] = "Initial with Name is Missing / Invalid";
        }

        if (!isset($_POST['inputnumber']) || strlen(trim($_POST['inputnumber'])) < 1)  {
            $errors[] = "Phone Number is Missing / Invalid";
        }

        if (!isset($_POST['inputEmail']) || strlen(trim($_POST['inputEmail'])) < 1)  {
            $errors[] = "Email is Missing / Invalid";
        }

        if (!isset($_POST['inputPassword']) || strlen(trim($_POST['inputPassword'])) < 1)  {
            $errors[] = "Pin / Password is Missing / Invalid";
        }

        if (!isset($_POST['inputregnum']) || strlen(trim($_POST['inputregnum'])) < 1)  {
            $errors[] = "Registration Number is Missing / Invalid";
        }

        if (!isset($_POST['inputdep']) || strlen(trim($_POST['inputdep'])) < 1)  {
            $errors[] = "Department is Missing / Invalid";
		}
		
		if (!isset($_POST['inputyear']) || strlen(trim($_POST['inputyear'])) < 1)  {
            $errors[] = "Academic Year is Missing / Invalid";
        }
        
        if (isset($_POST['inputEmail']) && strlen(trim($_POST['inputEmail'])) > 1) {
			
			$searchemail = mysqli_real_escape_string($connection, $_POST['inputEmail']);
        
            $querysearchemail = "SELECT * FROM users WHERE Email = '{$searchemail}'";
        
            $result_setsearchemail = mysqli_query($connection, $querysearchemail);
        
            verify_query($result_setsearchemail);
       
            $searchemailcount = mysqli_num_rows($result_setsearchemail);
        
            if ($searchemailcount > 0) {
                $errors[] = "Email is Already Exist";
            }
		}
		
		if (isset($_POST['inputPassword']) && strlen(trim($_POST['inputPassword'])) > 1) {
			
			$inputPassword = mysqli_real_escape_string($connection, $_POST['inputPassword']);
        
            $querysearchePassword = "SELECT * FROM users WHERE PIN = '{$inputPassword}'";
        
            $result_setsearchPassword = mysqli_query($connection, $querysearchePassword);
        
            verify_query($result_setsearchPassword);
       
            $searchPasswordcount = mysqli_num_rows($result_setsearchPassword);
        
            if ($searchPasswordcount > 0) {
                $errors[] = "Password / Pin is Already Exist";
            }
        }

        if (empty($errors)) {

            $inputname = mysqli_real_escape_string($connection, $_POST['inputname']);
            $inputinitials = mysqli_real_escape_string($connection, $_POST['inputinitials']);
            $inputnumber = mysqli_real_escape_string($connection, $_POST['inputnumber']);
			$inputEmail = mysqli_real_escape_string($connection, $_POST['inputEmail']);
            $inputPassword = mysqli_real_escape_string($connection, $_POST['inputPassword']);
            $inputregnum = mysqli_real_escape_string($connection, $_POST['inputregnum']);
			$inputdep = mysqli_real_escape_string($connection, $_POST['inputdep']);
			$inputyear = mysqli_real_escape_string($connection, $_POST['inputyear']);
			$position = "Student";
            
            $query = "INSERT INTO users (Full_Name, Name_With_Initials, Phone_Number, Email, PIN, Registration_Number, Department, Year, Position)
                VALUES ('{$inputname}', '{$inputinitials}', '{$inputnumber}', '{$inputEmail}', '{$inputPassword}', '{$inputregnum}', '{$inputdep}', '{$inputyear}', '{$position}') LIMIT 1 ";

            $result_setreg = mysqli_query($connection, $query);
            verify_query($result_setreg);

            $querycount2 = "SELECT * FROM users";
            $result_setcount2 = mysqli_query($connection, $querycount2);
            verify_query($result_setcount2);
            $count2 = mysqli_num_rows($result_setcount2);
            $count_different = $count2 - $count1;

            if($count_different < 0) {
                echo "<script type='text/javascript'>alert('Failed !');</script>";
            }

            elseif($count_different > 0) {
                echo "<script type='text/javascript'>alert('Sucessfull Added !');</script>";
            }
           
            else {
                $errors[] = 'Invalid Event Occurred';
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "  ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }

?>

<?php

	if(isset($_POST['teacher_Register'])) {
       
        $errors = array();

        if (!isset($_POST['t_inputname']) || strlen(trim($_POST['t_inputname'])) < 1)  {
            $errors[] = "Input Name is Missing / Invalid";
        }

        if (!isset($_POST['t_inputinitials']) || strlen(trim($_POST['t_inputinitials'])) < 1)  {
            $errors[] = "Initial with Name is Missing / Invalid";
        }

        if (!isset($_POST['t_inputnumber']) || strlen(trim($_POST['t_inputnumber'])) < 1)  {
            $errors[] = "Phone Number is Missing / Invalid";
        }

        if (!isset($_POST['t_inputEmail']) || strlen(trim($_POST['t_inputEmail'])) < 1)  {
            $errors[] = "Email is Missing / Invalid";
        }

        if (!isset($_POST['t_inputPassword']) || strlen(trim($_POST['t_inputPassword'])) < 1)  {
            $errors[] = "Pin / Password is Missing / Invalid";
        }

        if (!isset($_POST['t_inputRegNo']) || strlen(trim($_POST['t_inputRegNo'])) < 1)  {
            $errors[] = "Registration Number is Missing / Invalid";
        }

        if (!isset($_POST['t_inputFaculty']) || strlen(trim($_POST['t_inputFaculty'])) < 1)  {
            $errors[] = "Faculty is Missing / Invalid";
		}
        
        if (isset($_POST['t_inputEmail']) && strlen(trim($_POST['t_inputEmail'])) > 1) {
			
			$tsearchemail = mysqli_real_escape_string($connection, $_POST['t_inputEmail']);
        
            $tquerysearchemail = "SELECT * FROM users WHERE Email = '{$tsearchemail}'";
        
            $tresult_setsearchemail = mysqli_query($connection, $tquerysearchemail);
        
            verify_query($tresult_setsearchemail);
       
            $tsearchemailcount = mysqli_num_rows($tresult_setsearchemail);
        
            if ($tsearchemailcount > 0) {
                $errors[] = "Email is Already Exist";
            }
		}
		
		if (isset($_POST['t_inputPassword']) && strlen(trim($_POST['t_inputPassword'])) > 1) {
			
			$tinputPassword = mysqli_real_escape_string($connection, $_POST['t_inputPassword']);
        
            $tquerysearchePassword = "SELECT * FROM users WHERE PIN = '{$tinputPassword}'";
        
            $tresult_setsearchPassword = mysqli_query($connection, $tquerysearchePassword);
        
            verify_query($tresult_setsearchPassword);
       
            $tsearchPasswordcount = mysqli_num_rows($tresult_setsearchPassword);
        
            if ($tsearchPasswordcount > 0) {
                $errors[] = "Password / Pin is Already Exist";
            }
        }

        if (empty($errors)) {

            $t_inputname = mysqli_real_escape_string($connection, $_POST['t_inputname']);
            $t_inputinitials = mysqli_real_escape_string($connection, $_POST['t_inputinitials']);
            $t_inputnumber = mysqli_real_escape_string($connection, $_POST['t_inputnumber']);
			$t_inputEmail = mysqli_real_escape_string($connection, $_POST['t_inputEmail']);
            $t_inputPassword = mysqli_real_escape_string($connection, $_POST['t_inputPassword']);
            $t_inputRegNo = mysqli_real_escape_string($connection, $_POST['t_inputRegNo']);
			$t_inputFaculty = mysqli_real_escape_string($connection, $_POST['t_inputFaculty']);
			$tposition = "Teacher";
            
            $query = "INSERT INTO users (Full_Name, Name_With_Initials, Phone_Number, Email, PIN, Registration_Number, Faculty, Position)
                VALUES ('{$t_inputname}', '{$t_inputinitials}', '{$t_inputnumber}', '{$t_inputEmail}', '{$t_inputPassword}', '{$t_inputRegNo}', '{$t_inputFaculty}', '{$tposition}') LIMIT 1 ";

            $result_setreg = mysqli_query($connection, $query);
            verify_query($result_setreg);

            $querycount3 = "SELECT * FROM users";
            $result_setcount3 = mysqli_query($connection, $querycount3);
            verify_query($result_setcount3);
            $count3 = mysqli_num_rows($result_setcount3);
            $count_different = $count3 - $count1;

            if($count_different < 0) {
                echo "<script type='text/javascript'>alert('Failed !');</script>";
            }

            elseif($count_different > 0) {
                echo "<script type='text/javascript'>alert('Sucessfull Added !');</script>";
            }
           
            else {
                $errors[] = 'Invalid Event Occurred';
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "  ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }

?>



<div class="modal" tabindex=-1 role="dialog" id="chooseuser">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary text-white">
				
				<h4 class="modal-title">Add User</h4>

				<button type="button" class="close" data-dismiss="modal" area-label="close">

					<span area-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#usradd">Student</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tchradd">Teacher</button>
			</div>

		</div>
	</div>
</div>

<!-- -----Student------ -->

<div class="modal" tabindex=-1 role="dialog" id="usradd">
	<div class="modal-dialog" role="document">
		
		<div class="modal-content">
			
			<div class="modal-header bg-secondary text-white">
				
				<h4 class="modal-title">Add student</h4>

				<button type="button" class="close" data-dismiss="modal" area-label="close">

					<span area-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-body">
				
				<form action="" method="POST">

					<div class="form-group">
						<label for="inputname">Full name</label>
						<input type="text" class="form-control" name="inputname" placeholder="full name">
					</div>

					<div class="form-group">
						<label for="inputinitials">Name with intials</label>
						<input type="text" class="form-control" name="inputinitials" placeholder="Perera A.B.C">
					</div>

					<div class="form-group">
						<label for="inputnumber">Phone number</label>
						<input type="text" class="form-control" name="inputnumber" placeholder="+947********">
					</div>

					<div class="form-row">

						<div class="form-group col-md-6">
							<label for="inputEmail">Email</label>
							<input type="email" class="form-control" name="inputEmail" placeholder="****gmail.com">
						</div>

						<div class="form-group col-md-6">
							<label for="inputPassword">PIN</label>
							<input type="password" class="form-control" name="inputPassword" placeholder="****">
						</div>

					</div>
				
					<div class="form-row">
									
						<div class="form-group col-md-6">
							<label for="inputregnum">Registration number</label>
							<input type="text" class="form-control" name="inputregnum" placeholder="2406">
						</div>
									
						<div class="form-group col-md-4">
							<label for="inputdep">Department</label>
										
							<select name="inputdep" class="form-control">
								<option selected>Dept</option>
								<option>APC</option>
								<option>APS</option>
								<option>APN</option>
							</select>		
						</div>
									
						<div class="form-group col-md-2">
							<label for="inputyear">Year</label>
							<select name="inputyear" class="form-control">
								<option selected>year</option>
								<option>17</option>
								<option>16</option>
								<option>15</option>
							</select>
						</div>
					</div>

					<div class="modal-footer">	
						<button type="submit" name="student_Register" class="btn btn-primary">Register</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>


	<!--Teacher-->

<div class="modal" tabindex=-1 role="dialog" id="tchradd">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			
			<div class="modal-header bg-secondary text-white">
				<h4 class="modal-title">Add Lecturer</h4>

				<button type="button" class="close" data-dismiss="modal" area-label="close">

					<span area-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-body">
				
				<form action="" method="POST">

					<div class="form-group">
						<label for="name">Full name</label>
						<input type="text" class="form-control" name="t_inputname" placeholder="full name">
					</div>

					<div class="form-group">
						<label for="inputAddress">Name with intials</label>
						<input type="text" class="form-control" name="t_inputinitials" placeholder="Perera A.B.C">
					</div>

					<div class="form-group">
						<label for="inputAddress">Phone number</label>
						<input type="text" class="form-control" name="t_inputnumber" placeholder="+947********">
					</div>

					<div class="form-row">
						
						<div class="form-group col-md-6">
							<label for="inputEmail4">Email</label>
							<input type="email" class="form-control" name="t_inputEmail" placeholder="****gmail.com">
						</div>

						<div class="form-group col-md-6">
							<label for="inputPassword4">PIN</label>
							<input type="password" class="form-control" name="t_inputPassword" placeholder="****">
						</div>

					</div>

					<div class="form-row">
						
						<div class="form-group col-md-6">
							<label for="inputRegNo">Registraion number</label>
							<input type="text" class="form-control" name="t_inputRegNo" placeholder="----------">
						</div>

						<div class="form-group col-md-6">
							<label for="inputFaculty">Faculty</label>
							<input type="text" class="form-control" name="t_inputFaculty" placeholder="Applied Sciences">
						</div>

					</div>
													
					<div class="modal-footer">
						<button type="submit" name="teacher_Register" class="btn btn-primary">Register</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>