
<!--user login model-->

<div class="modal" tabindex=-1 role="dialog" id="basic">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header bg-secondary text-white">
					<h4 class="modal-title">PIN</h4>

					<button type="button" class="close" data-dismiss="modal" area-label="close">

						<span area-hidden="true">&times;</span>

					</button>

					
				</div>

				<div class="modal-body">
					<form action="" method="POST">
						
						<div class=" form-group">
							<label for="inputPw">Enter your pin</label>
							<input type="password" class="form-control" name = "pin" id="examplePw" placeholder="****">

						</div>

						<button type="button" class="btn btn-danger" data-dismiss="modal">close</button>
						<button type="submit" name="submit" class="btn btn-success">Log In</button>

					</form>
				</div>

			</div> 

		</div>
	</div>