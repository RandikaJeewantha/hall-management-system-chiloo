<?php

	function verify_query($result_set) {

		global $connection;

		if (!$result_set) {

			die("Database query failed!" . mysqli_error($connection));
		}
		else{
			return true;
		}
	}

	function check_req_fields($req_fields) {

		$errors = array();

		foreach ($req_fields as $field) {

			if (empty(trim($_POST[$field]))) {

				$errors[] = $field.' is required.';
			}
		}

		return $errors;
	}

	function check_max_len($max_len_fields) {

		$errors = array();

		foreach ($max_len_fields as $field => $max_len) {

			if (strlen(trim($_POST[$field])) > $max_len) {

				$errors[] = $field.' must be less than '.$max_len.' characters.';
			}
		}

		return $errors;
	}

	function row_count($query) {

		global $connection;
		$num_row = array();
		
		$result_set = mysqli_query($connection, $query);

		if ($result_set) {
			$num_row = mysqli_num_rows($result_set);
		}

		return $num_row;
	}

	function user_login() {

		global $connection;

		if(isset($_POST['submit'])) {

			$errors = array();
	
			if (!isset($_POST['pin']) || strlen(trim($_POST['pin'])) < 1)  {
				$errors[] = "Pin is Missing / Invalid";
			}
	
			if (empty($errors)) {
				$pin = mysqli_real_escape_string($connection, $_POST['pin']);
	
				$query = "SELECT * FROM users WHERE PIN = '{$pin}' LIMIT 1";
				$result_set = mysqli_query($connection, $query);
	
				verify_query($result_set);
	
				if (mysqli_num_rows($result_set) == 1) {
					$user = mysqli_fetch_assoc($result_set);
					$_SESSION['Name_With_Initials'] = $user['Name_With_Initials'];
					$_SESSION['Id'] = $user['Id'];
					$_SESSION['Position'] = $user['Position'];
	
					header('Location: user.php');
				}
	
				else {
					$errors[] = 'Invalid Pin';
				}
			}
			if (!empty($errors)) {
				
				$err = "";

				foreach ($errors as $error) {
					$err .= $error;
					$err .= "   ";
				}

				echo "<script type='text/javascript'>alert('$err');</script>";
			}
		}	
	}

	function admin_login() {

		global $connection;

		if(isset($_POST['submit_admin'])) {

			$errors = array();
	
			if (!isset($_POST['email']) || strlen(trim($_POST['email'])) < 1)  {
				$errors[] = "Email is Missing / Invalid";
			}
	
			if (!isset($_POST['password']) || strlen(trim($_POST['password'])) < 1)  {
				$errors[] = "Password is Missing / Invalid";
			}
	
			if (empty($errors)) {
				$email = mysqli_real_escape_string($connection, $_POST['email']);
				$password = mysqli_real_escape_string($connection, $_POST['password']);
	
				$query = "SELECT * FROM admins WHERE Email = '{$email}' AND Password = '{$password}' LIMIT 1";
				$result_set = mysqli_query($connection, $query);
	
				verify_query($result_set);
	
				if (mysqli_num_rows($result_set) == 1) {
					$user = mysqli_fetch_assoc($result_set);
					$_SESSION['Name_With_Initials'] = $user['Name_With_Initials'];
					$_SESSION['Id'] = $user['Id'];
					$_SESSION['Position'] = $user['Position'];

					header('Location: admin.php');
				}
	
				else {
					$errors[] = 'Invalid Email/Password';
				}
			}
			if (!empty($errors)) {
				
				$err = "";

				foreach ($errors as $error) {
					$err .= $error;
					$err .= "   ";
				}

				echo "<script type='text/javascript'>alert('$err');</script>";
			}
		}	
	}

?>
