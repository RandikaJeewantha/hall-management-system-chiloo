<!-- navigation bar-->

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	<a href="index.php" class="navbar-brand"> Hall Finder </a>

	<?php

		if(isset($_SESSION['Name_With_Initials'])) {
		 echo '<strong class="text-warning">Hello ! '. $_SESSION["Name_With_Initials"].'</strong>';
		}
		
	?>

	<button class="navbar-toggler" data-toggle="collapse" data-target="#ccsl">
			<span class="navbar-toggler-icon"></span>
	</button>

	<div class=" collapse navbar-collapse justify-content-end" id="ccsl" >

		<ul class="navbar-nav"> 

			<li class="nav-item active"> 
				<a href="index.php" class="nav-link"> Empty halls </a>
			</li>

			<li class="nav-item"> 
				<a href="user.php" class="nav-link"> User </a>
			</li>

			<li class="nav-item"> 
				<a href="admin.php" class="nav-link"> Admin </a>
			</li>

			<li class="nav-item"> 
				<button type="button" class="btn nav-link" data-toggle="modal" data-target="#loginchooseuser"><strong>Login</strong></button>
			</li>

			<li class="nav-item active"> 
				<a href="includes/logOut.php" class="nav-link"> Log Out </a>
			</li>

			<li class="nav-item">
			
				<form action="index.php" method="POST" class="form-inline my-2 my-lg-0">
				
					<input class="form-control mr-sm-2" type="search" name="searchHall" placeholder="Search" aria-label="Search" >
				
					<button class="btn btn-outline-light my-2 my-sm-0" type="submit" name= "searchhallsubmit" >Search</button>

				</form>
				
			</li>
		</ul>
	</div>
</nav>

<div class="modal" tabindex=-1 role="dialog" id="loginchooseuser">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-secondary text-white">
				
				<h4 class="modal-title"> Users Login </h4>

				<button type="button" class="close" data-dismiss="modal" area-label="close">

					<span area-hidden="true">&times;</span>

				</button>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#basic" class="close" data-dismiss="modal">User</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#admin" class="close" data-dismiss="modal">Admin</button>
			</div>

		</div>
	</div>
</div>