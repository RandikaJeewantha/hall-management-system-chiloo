<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>

<?php

	if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {

		echo "<script type='text/javascript'>alert('You cant access this page');</script>";
		echo "<script>setTimeout(\"location.href = 'index.php';\",0);</script>";
	}

?>

<?php 

    global $connection;
   
    if(isset($_POST['submitd'])) {

        $errors = array();

        if (!isset($_POST['code']) || strlen(trim($_POST['code'])) < 1)  {
            $errors[] = "Subject Code is Missing / Invalid";
        }

        if (!isset($_POST['Lecturer']) || strlen(trim($_POST['Lecturer'])) < 1)  {
            $errors[] = "Lecturer Name is Missing / Invalid";
        }

        if (!isset($_POST['hall']) || strlen(trim($_POST['hall'])) < 1)  {
            $errors[] = "Hall is Missing / Invalid";
        }

        if (!isset($_POST['Deaprtment']) || strlen(trim($_POST['Deaprtment'])) < 1)  {
            $errors[] = "Deaprtment is Missing / Invalid";
        }

        if (!isset($_POST['Date']) || strlen(trim($_POST['Date'])) < 1)  {
            $errors[] = "Date is Missing / Invalid";
        }

        if (!isset($_POST['StartTime']) || strlen(trim($_POST['StartTime'])) < 1)  {
            $errors[] = "Start Time is Missing / Invalid";
        }

        if (!isset($_POST['EndTime']) || strlen(trim($_POST['EndTime'])) < 1)  {
            $errors[] = "End Time is Missing / Invalid";
        }

        $queryall1 = "SELECT * FROM default_arrangement";
        $result_setall1 = mysqli_query($connection, $queryall1);
        verify_query($result_setall1);
        $count1 = mysqli_num_rows($result_setall1);

        if (empty($errors)) {
            $code = mysqli_real_escape_string($connection, $_POST['code']);
            $Lecturer = mysqli_real_escape_string($connection, $_POST['Lecturer']);
            $hall = mysqli_real_escape_string($connection, $_POST['hall']);
            $Deaprtment = mysqli_real_escape_string($connection, $_POST['Deaprtment']);
            $Date = mysqli_real_escape_string($connection, $_POST['Date']);
            $StartTime = mysqli_real_escape_string($connection, $_POST['StartTime']);
            $EndTime = mysqli_real_escape_string($connection, $_POST['EndTime']);

            $query = "INSERT INTO default_arrangement (Hall_Name, Day, Start_Time, End_Time, Lecturer, Subject, Department)
              VALUES ('{$hall}', '{$Date}', '{$StartTime}', '{$EndTime}', '{$Lecturer}', '{$code}', '{$Deaprtment}') LIMIT 1 ";

            $result_set = mysqli_query($connection, $query);
            verify_query($result_set);

            $queryall2 = "SELECT * FROM default_arrangement";
            $result_setall2 = mysqli_query($connection, $queryall2);
            verify_query($result_setall2);
            $count2 = mysqli_num_rows($result_setall2);
            $count_different = $count2 - $count1;

            if( $count_different > 0) {
                echo "<script type='text/javascript'>alert('Sucessfull added !');</script>";
            }
           
            else {
                $errors[] = 'Invalid Event Occurred';
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "   ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }
   
?>

<!DOCTYPE html>
<html lang="en"> 

  <head>
    <title>inserting</title>
    <?php require_once 'includes/header.php'; ?>
  </head>

  <body>

    <?php require_once 'includes/adminLog.php' ?>
    <?php require_once 'includes/userLogin.php' ?>
    <?php require_once 'includes/nav.php'; ?>

  <?php require_once 'includes/footer.php'; ?>

  <div class="container mt-5" >
			<div class="card-deck d-flex justify-content-center">
				<div class="card text-white bg-dark mb-3 mt-10 col-6" style="max-width: 50rem;">
			
					<div class="card-header">
						Insert Lectures
					</div>

					<div class="card-body">

						<form action="" method="POST">
							
							<div class=" form-goup">
							
								<label for="inputadd3">Lecture code</label>
								<select name="code" id="inputLectureCode" class="form-control">

									<option selected>Choose</option>
									<option>IS34543</option>
									<option>IS34541</option>
								</select>
						
							</div>
						
							<br>

							<div class=" form-goup">
						
								<label for="inputadd4">Lecturer</label>
								<select name="Lecturer" id="inputLecturer" class="form-control">

									<option selected>Choose</option>
									<option>Dr. B. T. G. S. Kumara</option>
									<option>Mr. R. L. Dangalla</option>
									<option>Mr. Kalinga Gunawardhana</option>
								</select>
							
								<br>
					
							</div>

							<div class=" form-goup">
							
								<label for="inputadd5">Lecture hall</label>
								<select name="hall" id="inputLecHall" class="form-control">

									<option selected>Choose</option>
									<option>NLH</option>
									<option>WH</option>
									<option>LT204</option>
								</select>
							
								<br>
						
							</div>

							<div class=" form-goup">
						
								<label for="inputadd6">Deaprtment</label>
								<select name="Deaprtment" id="inputdep" class="form-control">

									<option selected>Choose</option>
									<option>CIS</option>
									<option>PST</option>
									<option>SSC</option>
								</select>
							
								<br>
						
							</div>

					    	<div class="form-row">
								<div class="form-group col-md-4">
								
									<label for="inputdate">Day</label>
									<input type="text" name="Date" class="form-control" id="inputdate"  placeholder="Ex: Monday">
								</div>

								<div class="form-group col-md-4">
							
									<label for="inputStartTime">Starts at</label>
									<input type="time" name="StartTime" class="form-control" id="inputStartTime" placeholder="01:45 AM">
								</div>
						
								<div class="form-group col-md-4">
							
									<label for="inputEndTime">Ends at</label>
									<input type="time" name="EndTime" class="form-control" id="inputEndTime" placeholder="02:45 AM">
								</div>

							</div>
                          
							<div class="card-footer">

								<p> 	
									<button type="button" class="btn btn-danger" >Cancel</button>
										
									<!-- add alert to this -->
									<button type="submit" name="submitd" class="btn btn-success" >Save</button>
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

  </body>

</html>