<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php admin_login(); ?>
<?php

	if(!(isset($_SESSION['Name_With_Initials']))) {
		header('Location: index.php');
	}

?>

<?php 

    global $connection;
   
    if(isset($_POST['submit'])) {

        $errors = array();

        if (!isset($_POST['code']) || strlen(trim($_POST['code'])) < 1)  {
            $errors[] = "Subject Code is Missing / Invalid";
        }

        if (!isset($_POST['Lecturer']) || strlen(trim($_POST['Lecturer'])) < 1)  {
            $errors[] = "Lecturer Name is Missing / Invalid";
        }

        if (!isset($_POST['hall']) || strlen(trim($_POST['hall'])) < 1)  {
            $errors[] = "Hall is Missing / Invalid";
        }

        if (!isset($_POST['Deaprtment']) || strlen(trim($_POST['Deaprtment'])) < 1)  {
            $errors[] = "Deaprtment is Missing / Invalid";
        }

        if (!isset($_POST['Date']) || strlen(trim($_POST['Date'])) < 1)  {
            $errors[] = "Date is Missing / Invalid";
        }

        if (!isset($_POST['StartTime']) || strlen(trim($_POST['StartTime'])) < 1)  {
            $errors[] = "Start Time is Missing / Invalid";
        }

        if (!isset($_POST['EndTime']) || strlen(trim($_POST['EndTime'])) < 1)  {
            $errors[] = "End Time is Missing / Invalid";
        }

        if (!isset($_POST['optradio']) || strlen(trim($_POST['optradio'])) < 1)  {
            $errors[] = "Option is Missing / Invalid";
        }

        $queryall1 = "SELECT * FROM new_arrangement";
        $result_setall1 = mysqli_query($connection, $queryall1);
        verify_query($result_setall1);
        $count1 = mysqli_num_rows($result_setall1);

        if (empty($errors)) {
            $code = mysqli_real_escape_string($connection, $_POST['code']);
            $Lecturer = mysqli_real_escape_string($connection, $_POST['Lecturer']);
            $hall = mysqli_real_escape_string($connection, $_POST['hall']);
            $Deaprtment = mysqli_real_escape_string($connection, $_POST['Deaprtment']);
            $Date = mysqli_real_escape_string($connection, $_POST['Date']);
            $StartTime = mysqli_real_escape_string($connection, $_POST['StartTime']);
            $EndTime = mysqli_real_escape_string($connection, $_POST['EndTime']);
            $optradio = mysqli_real_escape_string($connection, $_POST['optradio']);

            if( $optradio == "extra" ) {
                $query = "INSERT INTO new_arrangement (Hall_Name, Date, Start_Time, End_Time, Lecturer, subject_code, Department)
                        VALUES ('{$hall}', '{$Date}', '{$StartTime}', '{$EndTime}', '{$Lecturer}', '{$code}', '{$Deaprtment}') LIMIT 1 ";
            }

            if( $optradio == "cansel" ) {
                $query = "DELETE FROM new_arrangement WHERE Hall_Name = '{$hall}' AND Date = '{$Date}' 
                        AND Start_Time = '{$StartTime}' AND End_Time = '{$EndTime}' LIMIT 1 ";
            }

            $result_set = mysqli_query($connection, $query);
            verify_query($result_set);

            $queryall2 = "SELECT * FROM new_arrangement";
            $result_setall2 = mysqli_query($connection, $queryall2);
            verify_query($result_setall2);
            $count2 = mysqli_num_rows($result_setall2);
            $count_different = $count2 - $count1;

            if( $optradio == "cansel" && $count_different < 0) {
                echo "<script type='text/javascript'>alert('Sucessfull deleted');</script>";
            }

            elseif( $optradio == "extra" && $count_different > 0) {
                echo "<script type='text/javascript'>alert('Sucessfull added');</script>";
            }
           
            else {
                $errors[] = 'Invalid Event Occurred';
            }

        }
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "   ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
        }
    }
   
?>

<!DOCTYPE html>
<html lang="en"> 

    <head>
        <title>User</title>
        <?php require_once 'includes/header.php'; ?>
    </head>

    <body>

        <?php require_once 'includes/adminLog.php' ?>
        <?php require_once 'includes/userLogin.php' ?>
        <?php require_once 'includes/nav.php'; ?>


		<div class="container mt-5" >
			<div class="card-deck d-flex justify-content-center">
				<div class="card text-white bg-dark mb-3 mt-10 col-6" style="max-width: 50rem;">
			
					<div class="card-header">
						Insert Extra/Cancelled Lectures
					</div>

					<div class="card-body">

						<form action="user.php" method="POST">
							
							<div class=" form-goup">
							
								<label for="inputadd3">Lecture code</label>
								<select name="code" id="inputLectureCode" class="form-control">

									<option selected>Choose</option>
									<option>IS34543</option>
									<option>IS34541</option>
								</select>
						
							</div>
						
							<br>

							<div class=" form-goup">
						
								<label for="inputadd4">Lecturer</label>
								<select name="Lecturer" id="inputLecturer" class="form-control">

									<option selected>Choose</option>
									<option>Dr. B. T. G. S. Kumara</option>
									<option>Mr. R. L. Dangalla</option>
									<option>Mr. Kalinga Gunawardhana</option>
								</select>
							
								<br>
					
							</div>

							<div class=" form-goup">
							
								<label for="inputadd5">Lecture hall</label>
								<select name="hall" id="inputLecHall" class="form-control">

									<option selected>Choose</option>
									<option>NLH</option>
									<option>WH</option>
									<option>LT204</option>
								</select>
							
								<br>
						
							</div>

							<div class=" form-goup">
						
								<label for="inputadd6">Deaprtment</label>
								<select name="Deaprtment" id="inputdep" class="form-control">

									<option selected>Choose</option>
									<option>CIS</option>
									<option>PST</option>
									<option>SSC</option>
								</select>
							
								<br>
						
							</div>

					    	<div class="form-row">
								<div class="form-group col-md-4">
								
									<label for="inputdate">Date</label>
									<input type="date" name="Date" class="form-control" id="inputdate"  placeholder="mm/dd/yyyy">
								</div>

								<div class="form-group col-md-4">
							
									<label for="inputStartTime">Starts at</label>
									<input type="time" name="StartTime" class="form-control" id="inputStartTime" placeholder="01:45 AM">
								</div>
						
								<div class="form-group col-md-4">
							
									<label for="inputEndTime">Ends at</label>
									<input type="time" name="EndTime" class="form-control" id="inputEndTime" placeholder="02:45 AM">
								</div>

							</div>

							<div class=" form-goup">
							
								<label for="inputadd5">Extra or Cancelled?
								</label>
							
								<br>
								
								<label class="radio-inline">
									<input type="radio" name="optradio" value="extra" checked> Extra
								</label>

								<label class="radio-inline">
									<input type="radio" name="optradio" value="cansel" checked> Cancelled
								</label>
							</div>
                          
							<div class="card-footer">

								<p> 	
									<button type="button" class="btn btn-danger" >Cancel</button>
										
									<!-- add alert to this -->
									<button type="submit" name="submit" class="btn btn-success" >Save</button>
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>				
		<?php require_once 'includes/footer.php'; ?>
    </body>
</html>
						