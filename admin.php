<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php 
	user_login(); 
?>

<?php

	if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {

		echo "<script type='text/javascript'>alert('You cant access this page');</script>";
		echo "<script>setTimeout(\"location.href = 'index.php';\",0);</script>";
	}

?>

<?php 
  	$querya = "SELECT Hall_Name FROM `new_arrangement` WHERE isConformed = 0 ORDER BY `new_arrangement`.`Date` ASC";
	$result_seta = mysqli_query($connection, $querya);
	verify_query($result_seta);
	$counta = mysqli_num_rows($result_seta); 
?>

<!DOCTYPE html>
<html lang="en"> 

    <head>
        <title>HOME</title>
        <?php require_once 'includes/header.php'; ?>
    </head>

    <body>
	
        <?php require_once 'includes/adminLog.php'; ?>
        <?php require_once 'includes/userLogin.php'; ?>
        <?php require_once 'includes/nav.php'; ?>
        <?php require_once 'includes/modifyAdmin.php'; ?>
        <?php require_once 'includes/userAdd.php'; ?>
        <?php require_once 'includes/ViewUsr.php'; ?>
        <?php require_once 'includes/confirmList.php'; ?>

<div class="container mt-5">

	<div class="card-deck">
		<div class="card bg-dark">
			<div class="card-body text-center text-white">
				<button type="button" class="btn btn-dark"  data-toggle="modal" data-target="#adminadd">Modify Admin Account</button>
			</div>
		</div>
		<div class="card bg-dark">
			<div class="card-body text-center text-white">
				<button type="button" class="btn btn-dark"  data-toggle="modal" data-target="#chooseuser">Modify/Add Users</button>
			</div>
		</div>
		<div class="card bg-dark">
			<div class="card-body text-center text-white">
				<button type="button" class="btn btn-dark"  data-toggle="modal" data-target="#extuser">View Existing Users </button>
			</div>
		</div>
	</div>

<!--<a class="btn btn-primary" href="#" role="button">Link</a>-->

	<div class="card-deck mt-5">
		<div class="card bg-dark">
		<div class="card-body text-center text-white">
			<a href="viewtable.php"><button type="button" class="btn btn-dark">View Shedule </button></a>
				
			</div>
		</div>
		<div class="card bg-dark">
			<div class="card-body text-center text-white">
				<a href="inserttable.php"><button type="button" class="btn btn-dark">Insert Table</button></a>
				
			</div>
		</div>
		<div class="card bg-dark">
			<div class="card-body text-center text-white">
				<button type="button" class="btn btn-dark"  data-toggle="modal" data-target="#request">Confirm modifications <span class="badge badge-danger"><?php echo $counta; ?></span></button>
			</div>
		</div>
	</div>
</div>
<?php require_once 'includes/footer.php'; ?>

    </body>
</html>
