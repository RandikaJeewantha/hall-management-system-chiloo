<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php 
	user_login(); 
?>

<?php

	if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {

		echo "<script type='text/javascript'>alert('You cant access this page');</script>";
		echo "<script>setTimeout(\"location.href = 'index.php';\",0);</script>";
	}

?>

<?php

    if(isset($_POST['edit'])) {
		
		$editableId = $_POST['edit'];

        $queryUsr= "SELECT * FROM users WHERE Id = '{$editableId}'";
        $result_setUsr = mysqli_query($connection, $queryUsr);
        verify_query($result_setUsr);
        $countUsr = mysqli_num_rows($result_setUsr);

        if($countUsr == 1){
			
			while ($data = mysqli_fetch_assoc($result_setUsr)){ 
				
				$name = $data["Full_Name"];
				$initials = $data["Name_With_Initials"];
				$number = $data["Phone_Number"];
				$Email = $data["Email"];
				$Password = $data["PIN"];
				$regnum = $data["Registration_Number"];
				$dep = $data["Department"];
				$year = $data["Year"];
				$position = $data["Position"];
		
            }

            if($position == "Student"){
        		$show_modal="s";
        		include("asserts/script.js.php"); 
            }
            elseif($position == "Teacher"){
                $show_modal="t";
				include("asserts/script.js.php");
			}
            else{
                echo "<script type='text/javascript'>alert('Error Occured !');</script>";
            }
        }
        else {
            echo "<script type='text/javascript'>alert('Server Error Occured !');</script>";
        }

    }
?>

<?php

	if(isset($_POST['student_edit'])) {
       
        $errors = array();
        
        if (isset($_POST['inputEmailE']) && strlen(trim($_POST['inputEmailE'])) > 1) {
			
			$searchemailE = mysqli_real_escape_string($connection, $_POST['inputEmailE']);
        
            $querysearchemailE = "SELECT * FROM users WHERE Email = '{$searchemailE}'";
        
            $result_setsearchemailE = mysqli_query($connection, $querysearchemailE);
        
            verify_query($result_setsearchemailE);
       
            $searchemailcountE = mysqli_num_rows($result_setsearchemailE);
        
            if ($searchemailcountE > 0) {
                $errors[] = "New Email is Already Exist";
            }
		}
		
		if (isset($_POST['inputPasswordE']) && strlen(trim($_POST['inputPasswordE'])) > 1) {
			
			$inputPasswordE = mysqli_real_escape_string($connection, $_POST['inputPasswordE']);
        
            $querysearchePasswordE = "SELECT * FROM users WHERE PIN = '{$inputPasswordE}'";
        
            $result_setsearchPasswordE = mysqli_query($connection, $querysearchePasswordE);
        
            verify_query($result_setsearchPasswordE);
       
            $searchPasswordcountE = mysqli_num_rows($result_setsearchPasswordE);
        
            if ($searchPasswordcountE > 0) {
                $errors[] = "New Password / Pin is Already Exist";
            }
        }

        if (empty($errors)) {

            $inputnameE = mysqli_real_escape_string($connection, $_POST['inputnameE']);
            $inputinitialsE = mysqli_real_escape_string($connection, $_POST['inputinitialsE']);
            $inputnumberE = mysqli_real_escape_string($connection, $_POST['inputnumberE']);
			$inputEmailE = mysqli_real_escape_string($connection, $_POST['inputEmailE']);
            $inputPasswordE = mysqli_real_escape_string($connection, $_POST['inputPasswordE']);
            $inputregnumE = mysqli_real_escape_string($connection, $_POST['inputregnumE']);
			$inputdepE = mysqli_real_escape_string($connection, $_POST['inputdepE']);
			$inputyearE = mysqli_real_escape_string($connection, $_POST['inputyearE']);
			$positionE = mysqli_real_escape_string($connection, $_POST['positionE']);

			echo "fff: ".$name;

			if (!isset($_POST['inputnameE']) || strlen(trim($_POST['inputnameE'])) < 1) {
				$inputnameE = $name;
			}

			if (!isset($_POST['inputinitialsE']) || strlen(trim($_POST['inputinitialsE'])) < 1) {
				$inputinitialsE = $initials;
			}

			if (!isset($_POST['inputnumberE']) || strlen(trim($_POST['inputnumberE'])) < 1) {
				$inputnumberE = $number;
			}

			if (!isset($_POST['inputEmailE']) || strlen(trim($_POST['inputEmailE'])) < 1) {
				$inputEmailE = $Email;
			}

			if (!isset($_POST['inputPasswordE']) || strlen(trim($_POST['inputPasswordE'])) < 1) {
				$inputPasswordE = $Password;
			}

			if (!isset($_POST['inputregnumE']) || strlen(trim($_POST['inputregnumE'])) < 1) {
				$inputregnumE = $regnum;
			}

			if (!isset($_POST['inputdepE']) || strlen(trim($_POST['inputdepE'])) < 1) {
				$inputdepE = $dep;
			}

			if (!isset($_POST['inputyearE']) || strlen(trim($_POST['inputyearE'])) < 1) {
				$inputyearE = $year;
			}

			if (!isset($_POST['positionE']) || strlen(trim($_POST['positionE'])) < 1) {
				$positionE = $position;
			}
            
            $query = "UPDATE users SET 
				Full_Name = '{$inputnameE}', 
				Name_With_Initials = '{$inputinitialsE}', 
				Phone_Number = '{$inputnumberE}', 
				Email = '{$inputEmailE}', 
				PIN = '{$inputPasswordE}', 
				Registration_Number = '{$inputregnumE}', 
				Department = '{$inputdepE}', 
				Year = '{$inputyearE}', 
				Position = '{$positionE}'
				WHERE Id = '{$editableId}' LIMIT 1 "
			;

			echo "nggg".$editableId;

            $result_setreg = mysqli_query($connection, $query);
            $is = verify_query($result_setreg);

            if($is) {
				echo "<script type='text/javascript'>alert('Successfull Edited !');</script>";
				echo "ng444gg".$editableId;
				echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
				echo "<script type='text/javascript'>alert('testing Edited !');</script>";
            }
           
            else {
				echo "<script type='text/javascript'>alert('Failed !');</script>";
				echo "ng000gg".$editableId;
				echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
				echo "<script type='text/javascript'>alert('testing Edited !');</script>";
            }

		}
		
        if (!empty($errors)) {
            
            $err = "";

            foreach ($errors as $error) {
                $err .= $error;
                $err .= "  ";
            }

            echo "<script type='text/javascript'>alert('$err');</script>";
			echo "<script>setTimeout(\"location.href = 'admin.php';\",0);</script>";
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php require_once 'includes/header.php'; ?>
		<title>Edit</title>
	</head>
	<body>

        <?php require_once 'includes/adminLog.php'; ?>
        <?php require_once 'includes/userLogin.php'; ?>
        <?php require_once 'includes/nav.php'; ?>

	<!-- -----Student------ -->

		<div class="modal" tabindex=-1 role="dialog" id="sEdit">
			<div class="modal-dialog" role="document">
				
				<div class="modal-content">
					
					<div class="modal-header bg-secondary text-white">
						
						<h4 class="modal-title">Add student</h4>

						<button type="button" class="close" data-dismiss="modal" area-label="close">

							<span area-hidden="true">&times;</span>

						</button>

					</div>

					<div class="modal-body">
						
						<form action="" method="POST">

							<div class="form-group">
								<label for="inputname">Full name</label>
								<input type="text" class="form-control" name="inputnameE" placeholder = "<?php echo $name; ?>" >
							</div> 

							<div class="form-group">
								<label for="inputinitials">Name with intials</label>
								<input type="text" class="form-control" name="inputinitialsE" placeholder = "<?php echo $initials; ?>" >
							</div>

							<div class="form-group">
								<label for="inputnumber">Phone number</label>
								<input type="text" class="form-control" name="inputnumberE" placeholder = "<?php echo $number; ?>" >
							</div>

							<div class="form-row">

								<div class="form-group col-md-6">
									<label for="inputEmail">Email</label>
									<input type="email" class="form-control" name="inputEmailE" placeholder = "<?php echo $Email; ?>" >
								</div>

								<div class="form-group col-md-6">
									<label for="inputPassword">PIN</label>
									<input type="password" class="form-control" name="inputPasswordE" placeholder = "<?php echo $Password; ?>" >
								</div>

							</div>
						
							<div class="form-row">
											
								<div class="form-group col-md-6">
									<label for="inputregnum">Registration number</label>
									<input type="text" class="form-control" name="inputregnumE" placeholder = "<?php echo $regnum; ?>" >
								</div>
											
								<div class="form-group col-md-4">
									<label for="inputdep">Department</label>
												
									<select name="inputdepE" class="form-control">
										<option selected><?php echo $dep; ?></option>
										<option>APC</option>
										<option>APS</option>
										<option>APN</option>
									</select>		
								</div>
											
							</div>

							<div class="form-row">

								<div class="form-group col-md-2">
									<label for="inputyear">Year</label>
									<select name="inputyearE" class="form-control">
										<option selected><?php echo $year; ?></option>
										<option>17</option>
										<option>16</option>
										<option>15</option>
									</select>
								</div>

								<div class="form-group col-md-2">
									<label for="positionE">Position</label>
									<select name="positionE" class="form-control">
										<option selected><?php echo $position; ?></option>
										<option>Teacher</option>
										<option>Student</option>
									</select>
								</div>

							</div>

							<div class="modal-footer">	
								<button type="submit" name="student_edit" class="btn btn-primary">Edit</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>


			<!--Teacher-->

		<div class="modal" tabindex=-1 role="dialog" id="tEdit">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					
					<div class="modal-header bg-secondary text-white">
						<h4 class="modal-title">Add Lecturer</h4>

						<button type="button" class="close" data-dismiss="modal" area-label="close">

							<span area-hidden="true">&times;</span>

						</button>

					</div>

					<div class="modal-body">
						
						<form action="" method="POST">

							<div class="form-group">
								<label for="name">Full name</label>
								<input type="text" class="form-control" name="t_inputname" placeholder="full name">
							</div>

							<div class="form-group">
								<label for="inputAddress">Name with intials</label>
								<input type="text" class="form-control" name="t_inputinitials" placeholder="Perera A.B.C">
							</div>

							<div class="form-group">
								<label for="inputAddress">Phone number</label>
								<input type="text" class="form-control" name="t_inputnumber" placeholder="+947********">
							</div>

							<div class="form-row">
								
								<div class="form-group col-md-6">
									<label for="inputEmail4">Email</label>
									<input type="email" class="form-control" name="t_inputEmail" placeholder="****gmail.com">
								</div>

								<div class="form-group col-md-6">
									<label for="inputPassword4">PIN</label>
									<input type="password" class="form-control" name="t_inputPassword" placeholder="****">
								</div>

							</div>

							<div class="form-row">
								
								<div class="form-group col-md-6">
									<label for="inputRegNo">Registraion number</label>
									<input type="text" class="form-control" name="t_inputRegNo" placeholder="----------">
								</div>

								<div class="form-group col-md-6">
									<label for="inputFaculty">Faculty</label>
									<input type="text" class="form-control" name="t_inputFaculty" placeholder="Applied Sciences">
								</div>

							</div>
															
							<div class="modal-footer">
								<button type="submit" name="teacher_edit" class="btn btn-primary">Register</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<?php require_once 'includes/footer.php'; ?>
	
	</body>
</html>