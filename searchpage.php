<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php 
	user_login(); 
?>

<?php

    if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {
        echo "<script type='text/javascript'>alert('You cant access this page');</script>";
        sleep(3);
        header('Location: index.php');
    }

?>

<?php

    global $connection;
    $searchedData = "";
    
	if(isset($_POST['search']) && !empty($_POST['searchingMail'])) {
    
    $errors = array();
      
    if (!isset($_POST['searchingMail']) || strlen(trim($_POST['searchingMail'])) < 1)  {
      $errors[] = "Email is Missing / Invalid";
    }
    
    if (isset($_POST['searchingMail']) && strlen(trim($_POST['searchingMail'])) > 1) {
			
		$searchingMail = mysqli_real_escape_string($connection, $_POST['searchingMail']);
        
        $querysearchingMail = "SELECT * FROM users WHERE Email = '{$searchingMail}'";
        
        $result_setsearchingMail = mysqli_query($connection, $querysearchingMail);
        
        verify_query($result_setsearchingMail);
       
        $searchingMailcount = mysqli_num_rows($result_setsearchingMail);
        
        if ($searchingMailcount == 0) {
            $errors[] = "There are no data about you entered email";
        }
    }
    
    if (!empty($errors)) {
            
      $err = "";

      foreach ($errors as $error) {
        $err .= $error;
        $err .= "  ";
      }

      echo "<script type='text/javascript'>alert('$err');</script>";

      header("Refresh: 5; url: admin.php");
    }

    else {
      $searchedData = $result_setsearchingMail;
    }

  }

?>

<?php 
  $query = "SELECT * FROM users";
	$result_set = mysqli_query($connection, $query);
  verify_query($result_set);
  $count1 = mysqli_num_rows($result_set); 
?>

<?php
  
  if(isset($_POST['delete'])) {

    $demail = $_POST['delete'];
    
    $queryd = "DELETE FROM users WHERE Email = '{$demail}'";
	  $result_setd = mysqli_query($connection, $queryd);
    verify_query($result_setd);

    $querycount2 = "SELECT * FROM users";
    $result_setcount2 = mysqli_query($connection, $querycount2);
    verify_query($result_setcount2);
    $count2 = mysqli_num_rows($result_setcount2);
    $count_different = $count2 - $count1;

    if($count_different < 0) {
      echo "<script type='text/javascript'>alert('Sucessfull deleted !');</script>";
      echo "<script>setTimeout(\"location.href = '';\",0);</script>";
    }

    elseif($count_different > 0) {
      echo "<script type='text/javascript'>alert('Failed !');</script>";
    }
           
    else {
      echo "<script type='text/javascript'>alert('Invalid Event Occurred !');</script>";
    }
  }

?>

<!DOCTYPE html>
<html lang="en"> 

    <head>
        <title>Tasks</title>
        <?php require_once 'includes/header.php'; ?>
    </head>

    <body>

        <?php require_once 'includes/adminLog.php'; ?>
        <?php require_once 'includes/userLogin.php'; ?>
        <?php require_once 'includes/nav.php'; ?>
        <?php require_once 'includes/modifyAdmin.php'; ?>
        <?php require_once 'includes/userAdd.php'; ?>
        <?php require_once 'includes/ViewUsr.php'; ?>
        <?php require_once 'includes/confirmList.php'; ?>

        <div class="container mt-5">
            
            <div>
                <form action="" method="POST" class="form-inline my-2 my-lg-0">

                    <input class="form-control mr-sm-2" type="search" name = "searchingMail" placeholder="re-enter correct e-mail" aria-label="Search" >
                    <!--<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">-->

                    <button type="submit" name="search" class="btn btn-outline-dark" >Search</button>
            
                </form>
            </div>

            <br><br>

            <table class="table">
    
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Full Name</th>
                        <th scope="col">Name With Initials</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Email</th>
                        <th scope="col">PIN</th>
                        <th scope="col">Registration Number</th>
                        <th scope="col">Department</th>
                        <th scope="col">Faculty</th>
                        <th scope="col">Year</th>
                        <th scope="col">Position</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                
                <tbody>

                    <?php
                        
                        if(!empty($searchedData)){
                            
                            $no = 0;
                            while ($data = mysqli_fetch_assoc($searchedData)){
                                $no = $no + 1;  
                                        
                                echo '
                                    <tr>
                                        <th scope="row">'.$no.'</th>
                                        <td>'.$data["Full_Name"].'</td>
                                        <td>'.$data["Name_With_Initials"].'</td>
                                        <td>'.$data["Phone_Number"].'</td>
                                        <td>'.$data["Email"].'</td>
                                        <td>'.$data["PIN"].'</td>
                                        <td>'.$data["Registration_Number"].'</td>
                                        <td>'.$data["Department"].'</td>
                                        <td>'.$data["Faculty"].'</td>
                                        <td>'.$data["Year"].'</td>
                                        <td>'.$data["Position"].'</td>

                                        <td>
                                            <form action="userEdit.php" method="post">
                                            <button type="submit" name="edit" class="btn btn-outline-dark" value='.$data["Id"].'>edit</button>
                                            </form>
                                        </td>

                                        <td>
                                            <form action="" method="post">
                                            <button type="submit" name="delete" class="btn btn-outline-dark" value='.$data["Email"].'>Delete</button>
                                            </form>
                                        </td>

                                    </tr>'
                                ;
                            }
                        }
                        else {
                            echo '
                                <tr>
                                    <th scope="row">**</th>
                                    <td>No Data Found !</td>
                                </tr>'
                            ;
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <?php require_once 'includes/footer.php'; ?>

    </body>
</html>