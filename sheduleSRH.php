<?php session_start(); ?>
<?php require_once('includes/connection.php'); ?>
<?php require_once('includes/functions.php'); ?>
<?php 
	user_login(); 
?>

<?php

    if(!(isset($_SESSION['Name_With_Initials'])) || $_SESSION['Position'] != "Admin") {
        echo "<script type='text/javascript'>alert('You cant access this page');</script>";
        sleep(3);
        header('Location: index.php');
    }

?>

<?php

    global $connection;
    $searchedData = "";
    
	if(isset($_POST['sheduleSearch']) && !empty($_POST['inputdate'])) {
        
        $errors = array();
      
        if (!isset($_POST['inputdate']) || strlen(trim($_POST['inputdate'])) < 1)  {
        $errors[] = "Date is Missing / Invalid";
        }
    
        if (isset($_POST['inputdate']) && strlen(trim($_POST['inputdate'])) > 1) {
                
            $inputdate = mysqli_real_escape_string($connection, $_POST['inputdate']);
            
            $queryinputdate = "SELECT * FROM new_arrangement WHERE Date = '{$inputdate}' ORDER BY `new_arrangement`.`Date` ASC";
            
            $result_setinputdate = mysqli_query($connection, $queryinputdate);
            
            verify_query($result_setinputdate);
        
            $searchinginputdatecount = mysqli_num_rows($result_setinputdate);
            
            if ($searchinginputdatecount == 0) {
                $errors[] = "There are no data about you entered email";
            }
        }
    
        if (!empty($errors)) {
                
        $err = "";

        foreach ($errors as $error) {
            $err .= $error;
            $err .= "  ";
        }

        echo "<script type='text/javascript'>alert('$err');</script>";

        header("Refresh: 5; url: viewtable.php");
        }

        else {
        $searchedData = $result_setinputdate;
        }

    }

?>

<!DOCTYPE html>
<html lang="en"> 

    <head>
        <title>Tasks</title>
        <?php require_once 'includes/header.php'; ?>
    </head>

    <body>

        <?php require_once 'includes/adminLog.php'; ?>
        <?php require_once 'includes/userLogin.php'; ?>
        <?php require_once 'includes/nav.php'; ?>
        <?php require_once 'includes/modifyAdmin.php'; ?>
        <?php require_once 'includes/userAdd.php'; ?>
        <?php require_once 'includes/ViewUsr.php'; ?>
        <?php require_once 'includes/confirmList.php'; ?>

        <div class="container mt-5">
            
            <div>
                <form action="" method="POST" class="form-inline">
				
                    <input type="date" class="form-control mr-2" name="inputdate"  placeholder="mm/dd/yyyy">
                    <button class="btn btn-dark my-2 my-sm-0" type="submit" name = "sheduleSearch" data-toggle="modal" data-target="">Search Date</button>
                      
                </form>
            </div>

            <br><br>

            <table class="table">
    
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Hall</th>
                        <th scope="col">Lecturer</th>
                        <th scope="col">Subject Code</th>
                        <th scope="col">Department</th>
                    </tr>
                </thead>
                
                <tbody>

                    <?php
                        
                        if(!empty($searchedData)){
                            
                            $no = 0;
                            while ($data = mysqli_fetch_assoc($searchedData)){
                                $no = $no + 1;  
                                        
                                echo '
                                    <tr>
                                        <th scope="row">'.$no.'</th>
                                        <td>'.$data["Date"].'</td>
                                        <td>'.$data["Start_Time"].'</td>
                                        <td>'.$data["End_Time"].'</td>
                                        <td>'.$data["Hall_Name"].'</td>
                                        <td>'.$data["Lecturer"].'</td>
                                        <td>'.$data["subject_code"].'</td>
                                        <td>'.$data["Department"].'</td>
                                    </tr>'
                                ;
                            }
                        }
                        else {
                            echo '
                                <tr>
                                    <th scope="row">**</th>
                                    <td>No Data Found !</td>
                                </tr>'
                            ;
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <?php require_once 'includes/footer.php'; ?>

    </body>
</html>